class Node {
  constructor(element = null, left = null, right = null) {
    this.data = element;
    this.left = left;
    this.right = right;
  }
}
class BST {
  constructor() {
    this.root = new Node();
  }

  add(element) {
  }

  findMin() {
  }

  findMax() {
  }

  has(data) {
  }

  remove(data) {
  }

  minHeight(node = this.root) {
  }

  maxHeight(node = this.root) {
  }

}
